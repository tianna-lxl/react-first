import React, {Component} from 'react'
import memoryUtils from '../../utils/memoryUtils'
import { Redirect } from 'react-router-dom'

import { Layout } from 'antd'

import LeftNav from '../../components/left-nav'
import Header from '../../components/header'

const { Footer, Sider, Content } = Layout

// 管理的路由组件
export default class Admin extends Component {
    render () {
        let user = memoryUtils.user
        console.log('user', user)
        if (!user || !user.id) {
            return <Redirect to="/login" />
        }
        return (
            <Layout style={{height: '100%'}}>
                <Sider>
                    <LeftNav />
                </Sider>
                <Layout>
                    <Header />
                    <Content style={{background: '#fff'}}>Content</Content>
                    <Footer style={{textAlign: 'center', color: '#ccc'}}>推荐使用谷歌浏览器，可以获得更佳页面操作体验</Footer>
                </Layout>
            </Layout>
        )
    }
}