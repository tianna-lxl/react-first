import React, {Component} from 'react'
import { Form, Input, Button, message } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'

import './login.less'
import logo from '../../assets/images/logo.png'

import { reqLogin } from '../../api'
import storageUtils from '../../utils/storageUtils'

// 登陆的路由组件
export default class Login extends Component {

    onFinish = async (values) => {
        console.log('表单内容为', values);

        let data = {
            code: "c8w1",
            password: "admin123",
            username: "admin",
            uuid: "dc311701b4c040c49d2102c69ab4c956"
        }
        let result = await reqLogin(data)
        if (result.code === 200) {
            this.props.history.replace('/')
            storageUtils.saveUser({
                id: 666,
                username: '小刘'
            })
            message.success('登陆成功!')
        } else {
            message.error(result.msg)
        }
    }

    validatorPwd = (rule, value) => {
        if (!value) {
            return Promise.reject(new Error('密码必须输入'))
        } else if (value.length < 4) {
            return Promise.reject(new Error('密码必须大于4位'))
        } else {
            return Promise.resolve()
        }
     }

    render () {
        return (
            <div className="login">
                <header className="login-header">
                    <img src={logo} alt="logo"/>
                    <h1>React项目：后台管理系统</h1>
                </header>
                <section className="login-content">
                    <h2>用户登陆</h2>
                    <Form
                        name="normal_login"
                        className="login-form"
                        onFinish={this.onFinish}>
                        <Form.Item
                            name="username"
                            rules={[
                                { required: true, whitespace: true, message: '用户名必须输入' },
                                { min: 4, message: '用户名至少4位' },
                                { max: 12, message: '用户名最多12位' },
                                { pattern: /^[a-zA-Z0-9_]+$/, message: '用户名必须是英文、数字或下划线组成' }
                            ]}
                            >
                            <Input 
                                prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} 
                                placeholder="用户名" 
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{ validator: this.validatorPwd }]}>
                            <Input
                                prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="密码"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                登陆
                            </Button>
                        </Form.Item>
                    </Form>
                </section>
            </div>
        )
    }
}

/* 
1.高阶函数
    1).一类特别的函数
        a.接受函数类型的参数
        b.返回值是函数
    2).常见
        a.定时器：setTimeout()/setInterval()
        b.Promise：Promise(() => {}) then(value => {}, reason => {})
        c.数组遍历相关的方法：forEach()/filter()/map()/reduce()/find()/findIndex()
        d.函数对象的bind()
    3).高阶函数更新动态，更具有扩展性
*/